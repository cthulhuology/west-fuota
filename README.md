FUOTA - AWS IoT Wireless Firmware Update Over the Air
------------------------------------------------------

This directory contains a python3.10 module that adds support for
adding FUOTA commands to the west cli tool for Zephyr.  This makes
it easier to build and test deployments of LoRaWAN firmware.  It
is not intended for large scale fleet management.

Getting Started
================

Before you can use the west fuota commands you need to install the
python requirements:

	pip install -r requirements.txt

Then to install this extension you can simply copy the fuota.py file to
	
	zephyr/scripts/west_commands/

And add the following yaml to your zephyr/scripts/west-commands.yml

  - file: scripts/west_commands/fuota.py
    commands:
      - name: fuota  
        class: FUOTA
        help: aws firmware update over the air commands


At this point you should see the fuota commands listed in 

	west help

The module doesn't load until you actually run the command so if any
of the dependencies are broken you won't discover that until then.


Bootstrapping AWS
=================

In order to enable AWS FUOTA you can use the "west fuota bootstrap"
command as a user with IAM role and policy creation capabilities:

	west fuota bootstrap

This will create an IAM Policy and Role that allows iotwireless to 
access S3 on your behalf, but you can generate the same using the
configuration managements tools of your choice.

The policy it creates is called WestFUOTAPolicy and has the value of:

	{
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Action": [
                        "s3:ListBucketVersions",
                        "s3:ListBucket",
                        "s3:GetObject"
                    ],
                    "Resource": [
                        "arn:aws:s3:::iotwirelessfwupdate/fwstation",
                        "arn:aws:s3:::iotwirelessfwupdate"
                    ]
                },
            ]
	}

The role named WestFUOTARole has and AssumeRolePolicyDocument of:

	{ 
            "Version": "2012-10-17",
            'Statement': [{
                'Effect': 'Allow',
                'Principal': {'Service': 'iotwireless.amazonaws.com'},
                'Action': 'sts:AssumeRole'
            }]
	}

These roles are 

