#!/usr/bin/env python
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
'''fuota.py

Commands for FUOTA updates with AWS IoT Wireless'''

from textwrap import dedent
from west.commands import WestCommand
from west import log
import boto3 as aws
import asyncio
import websockets
import json
import sys, os, datetime, hashlib, hmac, urllib.parse
import ssl
import logging
import uuid

def sign(key, msg):
    return hmac.new(key, msg.encode('utf-8'), hashlib.sha256).digest()

def getSignatureKey(key, dateStamp, regionName, serviceName):
    kDate = sign(('AWS4' + key).encode('utf-8'), dateStamp)
    kRegion = sign(kDate, regionName)
    kService = sign(kRegion, serviceName)
    kSigning = sign(kService, 'aws4_request')
    return kSigning

def sigv4(config_name):
    method = 'GET'
    service = 'iotwireless'
    region = os.environ.get('AWS_DEFAULT_REGION')
    host = 'api.iotwireless.' + region + '.amazonaws.com'
    endpoint = 'wss://api.iotwireless.' + region + '.amazonaws.com'
    access_key = os.environ.get('AWS_ACCESS_KEY_ID')
    secret_key = os.environ.get('AWS_SECRET_ACCESS_KEY')
    session_token = os.environ.get('AWS_SESSION_TOKEN')

    if access_key is None or secret_key is None or session_token is None or region is None:
        prin('You do not have any AWS credentials and region set in your environment')
        sys.exit()

    t = datetime.datetime.utcnow()
    amz_date = t.strftime('%Y%m%dT%H%M%SZ') # Format date as YYYYMMDD'T'HHMMSS'Z'
    datestamp = t.strftime('%Y%m%d') # Date w/o time, used in credential scope
    canonical_uri = '/start-network-analyzer-stream' 
    canonical_headers = 'host:' + host + '\n'
    signed_headers = 'host'
    algorithm = 'AWS4-HMAC-SHA256'
    credential_scope = datestamp + '/' + region + '/' + service + '/' + 'aws4_request'
    canonical_querystring = 'X-Amz-Algorithm=AWS4-HMAC-SHA256'
    canonical_querystring += '&X-Amz-Credential=' + urllib.parse.quote_plus(access_key + '/' + credential_scope)
    canonical_querystring += '&X-Amz-Date=' + amz_date
    canonical_querystring += '&X-Amz-Expires=300'
    canonical_querystring += '&X-Amz-Security-Token=' + urllib.parse.quote_plus(session_token)
    canonical_querystring += '&X-Amz-SignedHeaders=' + signed_headers
    canonical_querystring += '&configuration-name=' + config_name
    payload_hash = hashlib.sha256(('').encode('utf-8')).hexdigest()
    canonical_request = method + '\n' + canonical_uri + '\n' + canonical_querystring + '\n' + canonical_headers + '\n' + signed_headers + '\n' + payload_hash
    string_to_sign = algorithm + '\n' +  amz_date + '\n' +  credential_scope + '\n' +  hashlib.sha256(canonical_request.encode('utf-8')).hexdigest()

    signing_key = getSignatureKey(secret_key, datestamp, region, service)
    signature = hmac.new(signing_key, (string_to_sign).encode("utf-8"), hashlib.sha256).hexdigest()

    canonical_querystring += '&X-Amz-Signature=' + signature

    request_url = endpoint + canonical_uri + "?" + canonical_querystring 
    return request_url

class FUOTA(WestCommand):
    '''AWSCommand class provides utility functions to make FUOTA easier'''

    def __init__(self):
        self.iot = aws.client('iotwireless')
        self.iam = aws.client('iam')
        self.sts = aws.client('sts')
        super().__init__(
            'fuota',
            'aws firmware update over the air commands',
            dedent('''
            aws firmware update over the air commands

            * bootstrap - configures IAM roles and logging
            * task - creates a new FUOTA task uploading image
            * start - starts the FUOTA task
            * monitor - monitors the live progress of the task
            * stop - stops the update and deletes the FUOTA task
            '''))

    def do_add_parser(self,parser_adder):
        parser = parser_adder.add_parser(self.name,help=self.help,description=self.description)
        parser.add_argument('subcommand',nargs='*')
        return parser

    def do_run(self,args,unknown):
        match args.subcommand:
            case [ 'bootstrap' ]:
                self.bootstrap()
            case [ 'task', image, device ]:
                pass
            case [ 'start', taskId ]:
                pass
            case [ 'stop', taskId ]:
                pass
            case [ 'monitor', gateway, device ]:
                self.monitor(gateway,device)
            case _:
                log.inf(self.description)


    def bootstrap(self):

        policy_doc = {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Action": [
                        "s3:ListBucketVersions",
                        "s3:ListBucket",
                        "s3:GetObject"
                    ],
                    "Resource": [
                        "arn:aws:s3:::iotwirelessfwupdate/fwstation",
                        "arn:aws:s3:::iotwirelessfwupdate"
                    ]
                },
            ]}

        policy_resp = self.iam.create_policy(
            PolicyName='WestFUOTAPolicy',
            PolicyDocument=json.dumps(policy_doc),
            Description='Allows west to use iotwireless fuota')

        trust_doc = { 
            "Version": "2012-10-17",
            'Statement': [{
                'Effect': 'Allow',
                'Principal': {'Service': 'iotwireless.amazonaws.com'},
                'Action': 'sts:AssumeRole'
            }]}
        role_resp = self.iam.create_role(
            RoleName='WestFUOTARole',
            AssumeRolePolicyDocument=json.dumps(trust_doc),
            Description='Allows the west tool to use iotwireless fuota')

        print(role_resp['Role']['Arn'])
        attach_resp = self.iam.attach_role_policy(RoleName='WestFUOTARole',PolicyArn=policy_resp['Policy']['Arn'])

    def task(self,image,device):
        self.iot.create_fuota_task(
            LoRaWAN={ 'RfRegion': 'EU868' },
            FirmwareUpdateImage='image')

    def start(self,taskId):
        pass

    def stop(self,taskId):
        pass

    def monitor(self,gateway,device):
        logging.basicConfig(format="%(message)s",level=logging.DEBUG)
        self.config_name = 'west_'+uuid.uuid4().hex
        na_resp = self.iot.create_network_analyzer_configuration(
            Name=self.config_name,
            TraceContent={'WirelessDeviceFrameInfo': 'ENABLED', 'LogLevel': 'INFO' },
            WirelessDevices=[ device ],
            WirelessGateways=[ gateway ])
        asyncio.run(self.connection())
 
    async def connection(self):
        context = ssl.create_default_context()
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE
        try:
            while True:
                try:
                    url = sigv4(self.config_name)
                    async with websockets.connect( url, ssl=context, ping_interval=30,ping_timeout=30) as websocket:
                        while True:
                            message = await websocket.recv()
                            print(message)
                except websockets.exceptions.ConnectionClosedError as e:
                    print(f"Server disconnected unexpectedly {e}")
                except asyncio.exceptions.IncompleteReadError as e:
                    print(f"Framing error {e}")
                except websockets.exceptions.InvalidStatusCode as e:
                    print(f"Connection refused {e}")
                except websockets.exceptions.ConnectionClosedOK as e:
                    print(f"Connection closed by server {e}")
        finally:
            self.iot.delete_network_analyzer_configuration(ConfigurationName=self.config_name) 


